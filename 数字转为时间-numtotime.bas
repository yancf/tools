Attribute VB_Name = "numtotime"
Public Function tostringtime(ID As String) As String
   
Dim year As String
Dim month As String
Dim day As String
Dim hour As String
Dim mintue As String
Dim second As String


year = Mid(ID, 1, 4)
month = Mid(ID, 5, 2)
day = Mid(ID, 7, 2)
hour = Mid(ID, 9, 2)
mintue = Mid(ID, 11, 2)
second = Mid(ID, 13, 2)

tostringtime = year + "-" + month + "-" + day + " " + hour + ":" + mintue + ":" + second


End Function
