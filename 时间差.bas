Attribute VB_Name = "timecalc"
Function tomilltime(ID As String) As Double

Dim year As String
Dim month As String
Dim day As String
Dim hour As String
Dim mintue As String
Dim second As String
Dim re As String

year = Mid(ID, 1, 4)
month = Mid(ID, 5, 2)
day = Mid(ID, 7, 2)
hour = Mid(ID, 9, 2)
mintue = Mid(ID, 11, 2)
second = Mid(ID, 13, 2)

re = year + "-" + month + "-" + day + " " + hour + ":" + mintue + ":" + second

tomilltime = Val(hour) * 60 * 60 * 1000 + Val(mintue) * 60 * 1000 + Val(second) * 1000

End Function
